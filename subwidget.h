#ifndef SUBWIDGET_H
#define SUBWIDGET_H

#include <QWidget>
#include<QPushButton>

class subwidget : public QWidget
{
    Q_OBJECT
public:
    explicit subwidget(QWidget *parent = nullptr);
    void changeback();
signals:
    void BackSignals();
    void BackSignals(int);

public slots:
private:
    QPushButton subButton;
};

#endif // SUBWIDGET_H
