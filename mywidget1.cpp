#include "mywidget1.h"
#include"subwidget.h"
#include<QDebug>
#include"mybutton.h"
#include<QMenu>
#include<QMenuBar>
#include<QMainWindow>

MyWidget1::MyWidget1(QWidget *parent)
    : QWidget(parent)
{
    button.setParent(this);
    button.setText("CLOSE");
    button.move(200,200);
    connect(&button,&QPushButton::pressed,this,&MyWidget1::close);
    //动态分配空间的对象不用手动释放
    button_changename=new QPushButton(this);
    button_changename->setText("ABC");
    button_changename->move(300,300);
    connect(button_changename,&QPushButton::released,this,&MyWidget1::myslot);

    //connect(&openNew,&QPushButton::pressed,&sub,&MyWidget1::changeWin);
    /*button_changename:信号发出者，指针类型
     * &QPushButton::released发出的的信号类型 ， &发送者的类名::信号的名称
     * this 信号的接收者，指针类型
     * &MyWidget1::myslot 槽函数，信号处理函数。--信号入槽。 &接受的类名::槽函数的名字
     * ::域运算符
    */
    openNew.setParent(this);
    openNew.setText("打开新窗口");
    openNew.move(350,350);
    //sub.show();
    void (subwidget::*test)(int)=&subwidget::BackSignals;
    connect(&openNew,&QPushButton::released,this,&MyWidget1::changeWin);
    connect(&sub,test,this,&MyWidget1::print);
    resize(960,640);
    QPushButton *button3=new QPushButton(this);
    button3->setParent(this);
    button3->setText("init");
    //Lambda表达式：
    connect(button3,&QPushButton::released,
            [button3](){
        button3->setText("changed");
        qDebug()<<"test";
    }
            );





}

void MyWidget1::myslot(){
    if(button_changename->text()=="abc"){
        button_changename->setText("ABC");
    }else {
        button_changename->setText("abc");
    }
}
void MyWidget1::changeWin(){
    sub.show();
    this->hide();
}
void MyWidget1::backtomain(){
    sub.hide();
    this->show();
}
void MyWidget1::print(int a){
    qDebug()<<a<<endl;
}

MyWidget1::~MyWidget1()
{

}
