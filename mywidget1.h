#ifndef MYWIDGET1_H
#define MYWIDGET1_H

#include <QWidget>
#include<QPushButton>
#include"subwidget.h"

class MyWidget1 : public QWidget
{
    Q_OBJECT

public:
    MyWidget1(QWidget *parent = 0);
    ~MyWidget1();
    void myslot();
    void changeWin();
    void backtomain();
    void print(int a );
private:
    QPushButton button;
    QPushButton *button_changename;
    QPushButton openNew;
    QPushButton button2;
    subwidget sub;
};

#endif // MYWIDGET1_H
